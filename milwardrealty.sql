-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 22, 2016 at 03:17 am
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `milwardrealty`
--

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE IF NOT EXISTS `images` (
`id` int(10) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `property_id` int(10) NOT NULL,
  `count` int(10) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=62 ;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `filename`, `property_id`, `count`) VALUES
(42, '56a172e268332.jpg', 8, 1),
(43, '56a15c97b9ed1.jpg', 8, 2),
(49, '56a177b3be547.jpg', 25, 1),
(50, '56a177c2d5220.jpg', 25, 2),
(51, '56a177f42458e.jpg', 24, 1),
(53, '56a181d2862b8.jpg', 22, 1),
(55, '56a186c1df532.jpg', 22, 2),
(56, '56a186d9148dc.jpg', 22, 3),
(57, '56a186e221a67.jpg', 22, 4),
(58, '56a186eb78ba9.jpg', 22, 5),
(59, '56a186f50f9fa.jpg', 22, 6),
(60, '56a187b0132ae.jpg', 8, 3),
(61, '56a187bf0a9be.jpg', 8, 4);

-- --------------------------------------------------------

--
-- Table structure for table `properties`
--

CREATE TABLE IF NOT EXISTS `properties` (
`id` int(10) NOT NULL,
  `address` varchar(255) NOT NULL,
  `price` int(15) NOT NULL,
  `description` varchar(755) NOT NULL,
  `bedrooms` int(10) NOT NULL,
  `bathrooms` int(10) NOT NULL,
  `land` int(10) NOT NULL,
  `parking` int(10) NOT NULL,
  `region` varchar(255) NOT NULL,
  `filename` varchar(255) CHARACTER SET utf8mb4 NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `properties`
--

INSERT INTO `properties` (`id`, `address`, `price`, `description`, `bedrooms`, `bathrooms`, `land`, `parking`, `region`, `filename`) VALUES
(2, '468 EVANS BAY PARADE', 125000, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat. \r\n\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat. ', 6, 2, 200, 2, 'TAURANGA', ''),
(3, '12 YOOBEE AVENUE', 123000, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat.', 1, 1, 55, 0, 'WHAKATANE', ''),
(4, '49 PIHUNGA STREET', 100000, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat.', 3, 1, 80, 0, 'TAURANGA', ''),
(5, '1/5 EVA STREET', 345000, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat.', 6, 2, 500, 2, 'TAURANGA', ''),
(6, '46 RUGBY STREET', 210000, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat.', 4, 2, 110, 1, 'MOUNTMAUNGANUI', ''),
(8, '12 KENT TERRACE', 1000000, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore', 2, 1, 80, 3, 'TAURANGA', ''),
(22, 'changes applied', 2525, 'eetertertereryetyrtutyutyuiyu', 2, 2, 43346, 1, 'tauranga', '56a146d6f32e7.jpg'),
(23, 'fsgsfgdfh', 2147483647, 'dfgdfgdfgndf,n,dnfh,ndhndnfh,nd,hfn', 1, 2, 1465, 1, 'whakatane', ''),
(24, 'newly added right', 24234, 'teryetyrtyrtyrtyrty', 7, 5, 313, 2, 'whakatane', '56a130e6a80ee.jpg'),
(25, 'jkhjlhkl', 23232, 'wetwtryteryetytyrty', 2, 2, 1313, 1, 'whakatane', '56a14086919d8.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `property_image`
--

CREATE TABLE IF NOT EXISTS `property_image` (
  `property_id` int(10) NOT NULL,
  `image_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `regions`
--

CREATE TABLE IF NOT EXISTS `regions` (
`id` int(10) NOT NULL,
  `region` varchar(255) NOT NULL,
  `description` varchar(755) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `regions`
--

INSERT INTO `regions` (`id`, `region`, `description`) VALUES
(1, 'TAURANGA', 'Tauranga is the largest city in the Bay of Plenty and one of the fastest growing population centres in the country. It is also just a 15 minute drive from one of New Zealand''s most popular beach towns, Mount Maunganui.\r\n\r\nDowntown Tauranga has several historically significant areas to view during a scenic walk around the area. The Strand waterfront area is modern and always buzzing, and is home to a number of cafes, restaurants, pubs and nightclubs, as well as a range of accommodation options.'),
(2, 'MOUNTMAUNGANUI', '"The Mount" is the colloquial name for Mount Maunganui, a relaxed beach town that occupies a peninsula at the southern end of Tauranga Harbour. The peninsula is actually a huge sandbar, with a sheltered bay on the inner harbour side and a magnificent surf beach on the ocean side.\r\n\r\nAt the very tip of the peninsula is a dormant volcano - Mauao - which rises to 230 metres above sea level. There''s a choice of tracks leading to the summit, some more challenging than others. Huge views of the harbour, beach and Pacific Ocean make the effort totally worthwhile.'),
(3, 'WHAKATANE', 'The main centre for the eastern Bay of Plenty, Whakatane is one of the sunniest towns in New Zealand. Just off shore is White Island, a spectacular active volcano that can be visited by launch or helicopter. Guided tours of the island necessitate a gas mask and hard hat. Whakatane is also known for its fishing - more yellow fin tuna are caught here than anywhere else in the country.\r\n\r\nOther Whakatane attractions include the museum, an observatory and Maori historical sites. The Nga Tapuwae o Toi walkway leads to pa sites, native forest and seabird colonies. Dolphin and Seal tours, which can include swimming encounters, start in November. There are guided Kayak tours on offer around Ohiwa Harbour and Whale Island.'),
(4, 'KATIKATI', 'The rural town of Katikati was founded by settlers from Ulster in Ireland, but the district was populated by Maori long before Europeans arrived. The volcanic soils and sunny climate were as suitable then for kumara as they are today for kiwifruit and avocados.   \r\n\r\nIn recent years, Katikati has called itself the ''mural town''. More than 35 works of art in and around the main street - murals, sculptures and carvings - pay homage to the town''s early Maori and pioneer residents, and its timber milling and farming heritage. There is also a magnificent tribute to the men and women who left the district to fight in overseas wars. You can pick up a mural guide map from the information centre.'),
(5, 'KAWERAU', 'If you''re interested in the timber industry, Kawerau is your kind of town. It is surrounded by pine forest and the main employer in the area is the huge Tasman pulp and paper mill. You can tour the mill, but you need to book first.\r\n\r\nBehind the town is Putauaki (Mount Edgecumbe) - a dormant volcanic cone. By obtaining a permit, you can climb to the summit of the mountain.'),
(6, 'TEPUKE', 'Te Puke owes its prosperity to kiwifruit - a natural delicacy that''s brown and furry on the outside; bright green and juicy on the inside. Hundreds of local growers produce millions of kiwifruit, which are exported worldwide. You''ll also notice dairy, cattle, deer and sheep farms in the area. Honey and bee-related health products are produced here too. Te Puke''s visitor attractions includes a kiwifruit theme park. The town has a comprehensive shopping centre, a good choice of eating places, motels and a camping ground.');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(15) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` enum('user','admin') NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `role`) VALUES
(2, 'admin01', 'admin@milward.com', '$2y$10$NXNGaMZa6xGybYoVqzwAYeXrzYwAYYBWJNT3OfHDEiJ2.eOJhcCDu', 'admin'),
(3, 'user01', 'user@milward.com', '$2y$10$KpNpwtneuTvNpO65flhOrubYIc7U.d3rIya4SzedIZZWARBVfmjqq', 'user'),
(4, 'sssdsdsd', 's@m.com', '$2y$10$dnXNFvkf1/I1i9a.kZPiwuieLDUuWSvsNnJQULpnmFBWCuhGLViwu', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `images`
--
ALTER TABLE `images`
 ADD PRIMARY KEY (`id`), ADD KEY `property_id` (`property_id`);

--
-- Indexes for table `properties`
--
ALTER TABLE `properties`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `property_image`
--
ALTER TABLE `property_image`
 ADD KEY `property_id` (`property_id`), ADD KEY `image_id` (`image_id`);

--
-- Indexes for table `regions`
--
ALTER TABLE `regions`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `properties`
--
ALTER TABLE `properties`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `regions`
--
ALTER TABLE `regions`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(15) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `images`
--
ALTER TABLE `images`
ADD CONSTRAINT `images_ibfk_1` FOREIGN KEY (`property_id`) REFERENCES `properties` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
