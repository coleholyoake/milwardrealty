<?php 
  $errors = $property->errors; 
  $verb = ($property->id? "EDIT" : "ADD");

  if($property->id){
    $submitAction = ".\?page=updateproperty";
  } else {
    $submitAction = ".\?page=storeproperty";
  }
?>
<div class="container">
        <div class="row">
        
          <div class="col-sm-10 col-sm-offset-1">  
             <form id="addproperty" action="<?= $submitAction; ?>" method="POST" class="form-horizontal" enctype="multipart/form-data">
              <?php if($property->id): ?>
              <input type="hidden" name="id" value="<?= $property->id ?>">
              <?php endif; ?>
              <div class="row">
                <h1 class="authentication-title"><?= $verb; ?> PROPERTY</h1>
                <hr class="authentication-divider">
              </div>
      
               <div class="row"> 
                    <div class="form-group <?php if($errors['address']): ?> has-error <?php endif; ?>">
                        <label class="col-sm-2 col-sm-offset-1" for="address">Address</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="address" type="text" id="address" placeholder=""
                            value="<?php echo $property->address; ?>">
                            <div class="help-block"><?php echo $errors['address']; ?></div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="form-group <?php if($errors['region']): ?> has-error <?php endif; ?>">
                        <label class="col-sm-2 col-sm-offset-1" for="region">Region</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="region" type="text" id="region" placeholder=""
                            value="<?php echo $property->region; ?>">
                            <div class="help-block"><?php echo $errors['region']; ?></div>
                        </div>
                    </div>
                </div>

                <div class="row"> 
                    <div class="form-group <?php if($errors['price']): ?> has-error <?php endif; ?>">
                        <label class="col-sm-2 col-sm-offset-1" for="price">Price</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="price" type="number" id="price" placeholder=""
                            value="<?php echo $property->price; ?>">
                            <div class="help-block"><?php echo $errors['price']; ?></div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="form-group <?php if($errors['description']): ?> has-error <?php endif; ?>">
                        <label class="col-sm-2 col-sm-offset-1" for="description">Description</label>
                        <div class="col-sm-6">
                            <textarea class="form-control" id="text" name="description" rows="10"><?php echo $property->description; ?></textarea>
                            <div class="help-block"><?php echo $errors['description']; ?></div>
                        </div>
                    </div>
                </div>

                <div class="row"> 
                    <div class="form-group <?php if($errors['bedrooms']): ?> has-error <?php endif; ?>">
                        <label class="col-sm-2 col-sm-offset-1" for="bedrooms">Bedrooms</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="bedrooms" type="number" id="bedrooms" placeholder=""
                            value="<?php echo $property->bedrooms; ?>">
                            <div class="help-block"><?php echo $errors['bedrooms']; ?></div>
                        </div>
                    </div>
                </div>

                <div class="row"> 
                    <div class="form-group <?php if($errors['bathrooms']): ?> has-error <?php endif; ?>">
                        <label class="col-sm-2 col-sm-offset-1" for="bathrooms">Bathrooms</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="bathrooms" type="number" id="bathrooms" placeholder=""
                            value="<?php echo $property->bathrooms; ?>">
                            <div class="help-block"><?php echo $errors['bathrooms']; ?></div>
                        </div>
                    </div>
                </div>

                <div class="row"> 
                    <div class="form-group <?php if($errors['land']): ?> has-error <?php endif; ?>">
                        <label class="col-sm-2 col-sm-offset-1" for="land">Land (m&#0178)</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="land" type="number" id="land" placeholder=""
                            value="<?php echo $property->land; ?>">
                            <div class="help-block"><?php echo $errors['land']; ?></div>
                        </div>
                    </div>
                </div>

                <div class="row"> 
                    <div class="form-group <?php if($errors['parking']): ?> has-error <?php endif; ?>">
                        <label class="col-sm-2 col-sm-offset-1" for="parking">Parking</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="parking" type="number" id="parking" placeholder=""
                            value="<?php echo $property->parking; ?>">
                            <div class="help-block"><?php echo $errors['parking']; ?></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group <?php if($errors['filename']): ?> has-error <?php endif; ?>">
                        <label for="filename" class=" col-sm-2 col-sm-offset-1 control-label">Main Image </label>
                        <div class="col-sm-4">
                            <input type="file" id="filename" name="filename">        
                        </div>
                        <?php if($property->filename != ""): ?>
                        <div class="col-sm-offset-2 col-sm-1">
                            <img src="./images/photos/150h/<?=$property->filename;?>" alt="mainimage">
                        </div>
                        <div class="col-sm-2">
                            <div class="checkbox">
                                <label><input type="checkbox" name="removeImage" value="true"> Remove Photo</label>
                            </div>
                        </div>
                        <?php else: ?>
                        <div class="col-sm-2">
                            <p><i>No Photo found for this Property</i></p>
                        </div>
                        <?php endif; ?>
                    </div>
                </div> 

               <div class="form-group">
                 <div class="row">
                        <div class="btn-group col-md-2 col-md-offset-5">
                          <button type="submit" class="btn btn-default submitbutton"><small> <?= $verb; ?> PROPERTY </small></button>
                        </div>
                    </div>
               </div>
             </form> 
             <br><br>
</div>
        </div>


<?php if($property->id): ?>
  <div class="row">
          <div class="col-sm-10 col-sm-offset-1">  
             <form action=".\?page=deleteproperty" method="POST" class="form-horizontal">
              <div class="form-group">
              <div class="row">
                 <div class="btn-group col-md-offset-5 col-md-2">
                  <input type="hidden" name="id" value="<?= $property->id ?>">
                   <button type="submit" class="btn btn-default submitbutton"><small> REMOVE </small></button>
                 </div>
               </div>
              </div>
             </form> 
            </div>
            </div>

<?php endif; ?>  
</div>
<br><br>    
         
         
