<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-8 col-xs-offset-2">
            <!-- <embed class="center-block" id="homelogo" src="images/homelogo-animate.svg" onerror="this.src='images/png/homelogo-animate.png'"> -->
            <img class="center-block" id="homelogo" src="images/homelogo.svg" onerror="this.src='images/png/homelogo.png'">
        </div>
    </div>
</div>

        
<div id="searchbar">

    <div class="spacer"></div>

    <div class="container">
        <div class="row">
            <form method="GET" action="./" role="search">

                <div class="form-group">
                    <div class="col-md-3 col-md-offset-1 col-sm-6 select">
                        <input type="hidden" name="page" value="search">
                        <select class="form-control search" name="region">
                            <option value="region" disabled selected hidden>REGION &#9662</option>
                            <option value="tauranga">TAURANGA</option>
                            <option value="mountmaunganui">MOUNT MAUNGANUI</option>
                            <option value="whakatane">WHAKATANE</option>
                            <option value="katikati">KATIKATI</option>
                            <option value="mountmaunganui">KAWERAU</option>
                            <option value="whakatane">TE PUKE</option>
                        </select>
                    </div>
                    <div class="col-md-3 col-sm-6 select">
                        <select class="form-control search" name="bedrooms">
                            <option value="bedrooms" disabled selected hidden>BEDROOMS &#9662</option>
                            <option value="1">ONE</option>
                            <option value="2">TWO</option>
                            <option value="3">THREE</option>
                            <option value="4">FOUR</option>
                            <option value="5">FIVE</option>
                            <option value="6">SIX</option>
                        </select>
                    </div>
                </div>

                <div class="btn-group col-md-3 col-md-offset-1">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-default searchbutton">SEARCH</button>
                    </div>
                </div>

            </form>
        </div>
    </div>

    <div class="spacer"></div>

    <div class="container">
        <div class="row">
                <!-- <div class="col-xs-4 col-xs-offset-4"> -->
            <a href="#intro" class="more"><img class="center-block" src="images/arrowdown.svg" onerror="this.src='images/png/arrowdown.png'"></a>
                <!-- </div> -->
        </div>
    </div>
</div>


        


        



       
        
 









  





