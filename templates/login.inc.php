<?php 
  $errors = $user->errors;  

?>         

    <div class="container"> 
        <div class="row">
            <div class="col-md-2 col-md-offset-5 col-xs-4 col-xs-offset-4">
                <img class="center-block authentication-logo" src="images/homelogo.svg" onerror="this.src='images/png/homelogo.png'">
            </div>
        </div>
        <div class="row">
            <hr class="authentication-divider">
            <div class="col-xs-10 col-xs-offset-1">
                <h1 class="authentication-title">HELLO, PLEASE LOG IN HERE</h1>
            </div>
        </div>
    </div>

    <div id="about">
        <div class="container">  
            <form id="SignUpNewUser" action="./?page=auth.attempt" method="POST" class="form-horizontal">   

                <div class="row">
                    <div class="form-group col-md-12 <?php if($errors['username']): ?> has-error <?php endif; ?>">
                        <label class="col-lg-2 col-lg-offset-3 col-sm-3 col-md-offset-2" for="email">EMAIL</label>
                        <div class="col-lg-3 col-md-4 col-sm-8">
                            <input class="form-control" name="email" type="email" id="email" placeholder="example@mail.com" value="<?php echo $user->email; ?>">
                            <div class="help-block"><?php echo $errors['email']; ?></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-12 <?php if($errors['username']): ?> has-error <?php endif; ?>">
                        <label class="col-lg-2 col-lg-offset-3 col-sm-3 col-md-offset-2" for="password">PASSWORD</label>
                        <div class="col-lg-3 col-md-4 col-sm-8">
                            <input class="form-control" name="password" type="password" id="password" placeholder="*********">
                            <div class="help-block"><?php echo $errors['password']; ?></div>
                        </div>
                    </div>
                </div>
              
                <div class="form-group col-md-12">
                    <div class="row">
                        <div class="btn-group btn-group col-md-2 col-md-offset-5 col-sm-8 col-sm-offset-3">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default submitbutton"><small> LOG IN </small></button>
                        </div>
                        </div>
                    </div>
                </div>
                
            </form>
        </div>
    </div>


