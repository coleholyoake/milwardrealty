        <div id="property">
            <div class="container">
                <div class="row">
                    <div class="col-xs-2 col-xs-offset-5">
                        <br><br><br><br>
                        <img class="center-block authentication-logo" src="images/homelogo.svg" onerror="this.src='images/png/homelogo.png'">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                    <hr class="authentication-divider">
                        <h1 class="authentication-title">SEARCH RESULTS</h1>
                    </div>
                </div>
                <br><br>
            </div> 
            
                <?php if (count($properties) > 0): ?>
                <?php foreach($properties as $detail): ?>

            <div class="container">
                <div class="row property-border">
                    <div class="col-md-3 col-md-offset-1">
                        <img src="images/featured01.jpg">
                    </div>
                    <div class="col-md-7">
                        <a href=".\?page=property&amp;id=<?= $detail->id; ?>">
                            <div class="address-heading">
                                <div class="row">
                                    <div class="col-md-9">
                                        <h4><?=$detail->address;?> <br> <?=$detail->region;?></h4> 
                                    </div>
                                    <div class="col-md-3 price">
                                        <h4>$<?=$detail->price;?></h4> 
                                    </div>
                                </div>
                            </div>
                        </a>
                        <p class="ellipsis"><?=$detail->description;?></p>
                        <!-- <br> -->
                        <div class="row">
                            <div class="col-md-2 col-md-offset-8 bed-bath">
                                <p><img src="images/bedroom.svg" onerror="this.src='images/png/bedroom.png'"> <?=$detail->bedrooms;?></p>
                            </div>
                            <div class="col-md-2 bed-bath">
                                <p><img src="images/bathroom.svg" onerror="this.src='images/png/bathroom.png'"> <?=$detail->bathrooms;?></p>
                            </div>
                            
                        </div>
                    </div>  
                </div> 
                
                <br><br>
            </div>
            <?php endforeach; ?>

            <?php else: ?>
            <div>
                <h4 class="authentication-title space"><b><i>*Sorry there are no Properties that fit your requirements at the moment, please try again later...</i></b></h4>
                <?php endif; ?>
            </div>
        </div>
    </div>


        

