<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Milward Realty <?php if($page_title) { echo "- " . $page_title ;} ?></title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    </head>
    <body>
        
        
        
            <nav id="top" class="navbar navbar-default">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" id="hamburger" class="navbar-toggle collapsed" 
                        data-toggle="collapse" data-target="#collapsemenu"
                        aria-expanded="false">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a href=".\" class="navbar-brand">
                            <img src="images/logo.svg" alt="logo" onerror="this.src='images/png/logo.png'">
                        </a>
                    </div>
                    <div class="collapse navbar-collapse" id="collapsemenu">
                        <ul class="nav navbar-nav">
                            <li><a href=".\">HOME</a></li>
                            <li><a href=".\#region-nav">PROPERTY</a></li>
                            <li><a href=".\#properties-sold">ABOUT</a></li>
                            <li><a href=".\#contact-form">CONTACT</a></li>
                        </ul>
                        <hr class="divide">
                        <ul class="nav navbar-nav navbar-right">
                        
                        <?php if(! static::$auth->check()): ?>

                            <li <?php if ($page === "auth.login"): ?> class="active" <?php endif ?> ><a href=".\?page=login">LOG IN</a></li>
                            <li id="signup" <?php if ($page === "auth.signup"): ?> class="active" <?php endif ?> ><a href=".\?page=signup">SIGN UP</a></li>

                        <?php else: ?>
                        
                            <li><a href="#"><b><i><?= static::$auth->user()->username; ?></i></b></a></li>
                            <li <?php if ($page === "watchlist"): ?> class="active" <?php endif ?> ><a href=".\?page=watchlist">WATCHLIST</a></li>
                            <li id="signup"><a href=".\?page=logout">LOG OUT</a></li>
                            
                        <?php endif; ?>
                        
                        </ul>
                    </div>  
                </div>
            </nav>


        <?php $this->content(); ?>


        <div id="footer">
            <div class="container">
                <a href="#top" class="more"><img class="center-block" src="images/arrowup.svg" alt="to the top" onerror="this.src='images/png/arrowup.png'"></a>
                <div class="row">
                    <div class="col-md-2 col-md-offset-1 col-sm-3">
                        <a href=".\"><img class="footerimage" id="footerlogo" src="images/homelogo_white.svg" alt="logo" onerror="this.src='images/png/homelogo_white.png'"></a>
                    </div>
                    <div class="col-md-1 col-md-offset-2 col-sm-2 col-sm-offset-1 col-xs-3 col-xs-offset-1">
                        <ul>
                            <li><a href=".\">HOME</a></li>
                            <li><a href=".\#region-nav">PROPERTY</a></li>
                            <li><a href=".\#properties-sold">ABOUT</a></li>
                            <li><a href=".\#contact-form">CONTACT</a></li>
                        </ul>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-3">
                        <ul>
                            <li><a href=".\?page=error404">REVIEWS</a></li>
                            <li><a href=".\?page=error404">FAQ</a></li>
                            <li><a href=".\?page=error404">TESTIMONIALS</a></li>
                            <li><a href=".\?page=error404">MAP</a></li>
                        </ul>
                    </div>
                    <div class="col-md-2 col-md-offset-2 col-sm-3 col-sm-offset-1 col-xs-4 col-xs-offset-1s">
                        <div class="row">
                            <div class="col-xs-4">
                                <a href="https://www.facebook.com"><img class="footerimage" src="images/facebook.svg" alt="facebook" onerror="this.src='images/png/facebook.png'"></a>
                            </div>
                            <div class="col-xs-4">
                                <a href="https://twitter.com"><img class="footerimage" src="images/twitter.svg" alt="twitter" onerror="this.src='images/png/twitter.png'"></a>
                            </div>
                            <div class="col-xs-4">
                                <a href="https://www.linkedin.com"><img class="footerimage" src="images/linkedin.svg" alt="linkedin" onerror="this.src='images/png/linkedin.png'"></a>
                            </div>
                            <br><br>
                            <div id="keepup">
                                <p><i>Simply keep up to date by connecting with us!</i></p>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div id="copyright">
                    <p><small><?php echo date("Y"); ?> &copy; Milward Realty. All Rights Reserved. Designed by Cole Holyoake.</small></p>
                </div>
            </div>
        </div>
        


        

        



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/taggle.min.js"></script>
    
    <script src="js/main.js"></script>
    <script src="js/chart.js"></script>

  </body>
</html>




