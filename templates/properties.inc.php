<div id="region-nav">  
            <div class="container">
                <div class="row">
                    <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12">
                        <ul class="nav nav-pills nav-justified">
                          <li role="presentation" <?php if ($regionDetails->region === "TAURANGA"): ?> class="active" <?php endif ?>><a href=".\?page=home&amp;region=tauranga#region-nav" >TAURANGA</a></li>
                          <li role="presentation" <?php if ($regionDetails->region === "MOUNTMAUNGANUI"): ?> class="active" <?php endif ?>><a href=".\?page=home&amp;region=mountmaunganui#region-nav">MOUNT MAUNGANUI</a></li>
                          <li role="presentation" <?php if ($regionDetails->region === "WHAKATANE"): ?> class="active" <?php endif ?>><a href=".\?page=home&amp;region=whakatane#region-nav">WHAKATANE</a></li>
                          <li role="presentation" <?php if ($regionDetails->region === "KATIKATI"): ?> class="active" <?php endif ?>><a href=".\?page=home&amp;region=katikati#region-nav">KATIKATI</a></li>
                          <li role="presentation" <?php if ($regionDetails->region === "KAWERAU"): ?> class="active" <?php endif ?>><a href=".\?page=home&amp;region=kawerau#region-nav">KAWERAU</a></li>
                          <li role="presentation" <?php if ($regionDetails->region === "TEPUKE"): ?> class="active" <?php endif ?>><a href=".\?page=home&amp;region=tepuke#region-nav">TE PUKE</a></li>
                        </ul>
                    </div>
                </div>  


                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12">
                        <h2><?=$regionDetails->region;?></h2>
                        <p><?=$regionDetails->description;?></p>
                    </div>
                </div>
                <hr>
            </div>
        </div>

        <?php if(static::$auth->isAdmin()): ?>
        <div class="container">
            <div class="row addedit">
                <div class="btn-group col-md-2 col-xs-3 col-xs-offset-9">
                    <a href=".\?page=addproperty">
                        + Add Property
                    </a>
                </div>
            </div>
        </div>
        <br><br>        <?php endif; ?>


         
        <div id="property">
        <?php if (count($propertyDetails) > 0): ?>
        <?php foreach($propertyDetails as $detail): ?>
            <div class="container">
                <div class="row property-border">
                    <div class="col-md-3 col-md-offset-1">
                        <img src="./images/photos/600h/<?=$detail->filename;?>">
                    </div>
                    <div class="col-md-7">
                        <a href=".\?page=property&amp;id=<?= $detail->id; ?>">
                            <div class="address-heading">
                                <div class="row">
                                    <div class="col-xs-9">
                                        <h4><?=$detail->address;?> <br> <small class="white"><?=$detail->region;?></small></h4> 
                                    </div>
                                    <div class="col-xs-3 price">
                                        <h4>$<?=$detail->price;?></h4> 
                                    </div>
                                </div>
                            </div>
                        </a>
                        <p class="ellipsis"><?=$detail->description;?></p>
                        <!-- <br> -->
                        <div class="row">
                            <div class="col-xs-2 col-xs-offset-8 bed-bath">
                                <p><img src="images/bedroom.svg" onerror="this.src='images/png/bedroom.png'"> <?=$detail->bedrooms;?></p>
                            </div>
                            <div class="col-xs-2 bed-bath">
                                <p><img src="images/bathroom.svg" onerror="this.src='images/png/bathroom.png'"> <?=$detail->bathrooms;?></p>
                            </div>
                            
                        </div>
                    </div>  
                </div> 
                
                <br><br>
            </div>
            <?php endforeach; ?>

                <?php else: ?>
                <h4 class="authentication-title"><b><i>*Sorry there are no Properties on the market in this region, please try again later.</i></b></h4>
                <?php endif; ?>
        </div>    
     
        
        