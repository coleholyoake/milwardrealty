<div id="intro">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <p>Milward Realty’s aim is to give you the best service possible, whether that be from just an 
                        appraisal to selling your home. Our approach is honest, upfront and prompt. Our aim is to create 
                        a stress-free process that is as involved as you as the client wishes to be. </p>
                        <hr>
                    </div>
                </div>
            </div>
        </div>

        <div id="featured">
            <img src="images/featured01.jpg"><img src="images/featured03.jpg"><img src="images/featured02.jpg"><img src="images/featured01.jpg" id="featuredimage">
        </div>

