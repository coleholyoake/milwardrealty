<div id="property">
            <div class="container">
                <div class="row">
                    <div class="col-xs-2 col-xs-offset-5">
                        <br><br><br><br>
                        <img class="center-block authentication-logo" src="images/homelogo.svg" onerror="this.src='images/png/homelogo.png'">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                    <hr class="authentication-divider">
                        <h1 class="authentication-title">ERROR 404</h1>
                    </div>
                </div>
                <br><br>
            </div> 

            <div>
                <h4 class="authentication-title space"><b><i>*Sorry something went wrong...</i></b></h4>
               
            </div>
           </div>