

<div id="properties-sold">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-md-offset-1 col-sm-6 col-xs-5">
                <h1>PROPERTIES SOLD</h1>
                <ul>
                    <li>Tauranga <img src="images/badge_tauranga.svg" onerror="this.src='images/png/badge_tauranga.png'"></li>
                    <li>Mount Maunganui <img src="images/badge_mount.svg" onerror="this.src='images/png/badge_mount.png'"></li>
                    <li>Whakatane <img src="images/badge_whakatane.svg" onerror="this.src='images/png/badge_whakatane.png'"></li>
                    <li>Katikati <img src="images/badge_kati.svg" onerror="this.src='images/png/badge_kati.png'"></li>
                    <li>Kawerau <img src="images/badge_kawerau.svg" onerror="this.src='images/png/badge_kawerau.png'"></li>
                    <li>Te Puke <img src="images/badge_tepuke.svg" onerror="this.src='images/png/badge_tepuke.png'"></li>
                </ul>
            </div>
            <div class="col-md-5 col-sm-6 col-xs-7">
                <canvas id="myChart"></canvas>
            </div>
        </div>
    </div>
</div>

<div id="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12">
                <h2>ABOUT US</h2>
                <p>Milward Realty has an extensive knowledge of Bay of Plenty real estate and 
                in particular the Tauranga, Mount Maunganui and Te Puke real estate markets. We 
                have a dedicated team of property consultants, three offices and an efficient 
                support team. The team has a genuine commitment to their customers and is the 
                cornerstone of their real estate success in the Bay of Plenty area.
                <br><br>
                If you need further assistance with your real estate needs, please contact us today.</p>
            </div>
        </div>

        <hr id="contact-form">

        <div class="row">
            <form id="contact" action=".\?page=contact" method="POST" class="form-horizontal">
                <div class="form-group col-md-12">
                     <!-- <div class="container"> -->
                    
                        <label class="col-md-offset-1 col-sm-1" for="name">NAME</label>
                        <div class="col-md-3 col-sm-5">
                            <input class="form-control" type="text" id="name" name="name">
                        </div>
                    
        
                </div>
                <div class="form-group col-md-12">
                     <!-- <div class="container"> -->
                    
                        <label class="col-md-offset-1 col-sm-1" for="email">EMAIL</label>
                        <div class="col-md-3 col-sm-5">
                            <input class="form-control" type="email" id="email" name="email">
                        </div>
                        <label class="col-md-offset-1 col-sm-1" for="phone">PHONE</label>
                        <div class="col-md-3 col-sm-5">
                            <input class="form-control" type="text" id="phone" name="phone">
                        </div>
                    
        
                </div>
                <div class="form-group col-md-12">
                     <!-- <div class="container"> -->
                    
                        <label class="col-md-offset-1 col-sm-1" for="text">TEXT</label>
                        <div class="col-md-8 col-sm-11">
                            <textarea class="form-control" id="text" name="text" rows="8"></textarea>
                        </div>
                    
        
                </div>
                
                     <!-- <div class="container"> -->
                    
                    <div class="btn-group col-md-2 col-md-offset-5 col-sm-11 col-sm-offset-1">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default submitbutton"><small> SEND </small></button>
                        </div>
                    </div>
                    
        
                
            </form>
        </div>
    </div>
</div> 

    <div id="contact">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 col-md-offset-4 col-xs-4 col-xs-offset-2">
                        <img src="images/address.svg" onerror="this.src='images/png/address.png'"><br>
                        <p>468 Evans Bay Parade<br>
                        Hataitai<br>
                        Wellington, 6021</p>
                    </div>
                    <div class="col-md-2 col-xs-4">
                        <img src="images/contact.svg" onerror="this.src='images/png/contact.png'"><br>
                        <p>info@milwardrealty.co.nz<br><br>
                        (07) 378 3253</p>
                    </div>
                </div>
            </div>
        </div>
    


        