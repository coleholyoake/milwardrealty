
        <div class="propertyfocus">
            <div class="container">
                <div class="row property-border">
                    <!-- <div class="col-md-10 col-md-offset-1">
                        <ol class="breadcrumb">
                            <li><a href=".\"><small class="black">HOME</small></a></li>
                            <li><a href=".\#region-nav"><small class="black">PROPERTY</small></a></li>
                            <li class="active"><i class="black"><?= $property->address; ?></i></li>
                        </ol>
                    </div> -->
                    <div class="col-xs-7 col-xs-offset-1 propertyfocus-heading">
                        <h2><?= $property->address; ?><br> <small class="black"><?= $property->region; ?></small></h2> 
                    </div>
                    <div class="col-xs-3 price">
                        <h2>$<?= $property->price; ?></h2> 
                    </div>
                </div>
                <?php if(static::$auth->isAdmin()): ?>
                <div class="row addedit"><br>
                    <div class="btn-group col-md-2 col-md-offset-9 col-sm-offset-8 col-xs-3 col-xs-offset-9">
                        <a href=".\?page=editproperty&amp;id=<?= $property->id; ?>">
                            + Edit Property
                        </a>
                    </div>
                </div>
            <?php endif; ?>
                <br><br>
                <div class="row property-upper">
                    <div class="col-md-5 col-md-offset-1 col-sm-10 col-sm-offset-1"> 
                        <img src="./images/photos/600h/<?= $property->filename; ?>" class="mainimage">  
                    </div> 
                    <div class="col-md-4 col-md-offset-1 col-sm-10 col-sm-offset-1">
                        <p><?= $property->description; ?></p>
                    </div>
                </div>
            </div> 
        </div>

<!-- To check if property has got images or not. If it has got images, how many are uploaded ($imgCount)-->
<!-- Get the highest value of $imgCount ($tmp) -->
<!-- Note: Not more than 6 images could be uploaded -->
<!-- if the highest value of imgCount is greater than $count, then count takes that value and reumes with image upload. -->
        <?php 
            $count = (isset($_GET['count'])) ? $_GET['count'] : 0;
            $tmp = 0;
            $imgCount= 0; 

            if(count($imageDetails) > 0){
                foreach ($imageDetails as $img) {
                    $imgCount = $img->count;
                    if($tmp < $imgCount){
                        $tmp = $imgCount;
                    } 
                }

                if($tmp > $count){
                    $count = $tmp;
                }
            }
            $count++;
           
        ?>


<!-- Bootstrap collapse is used here.  -->

        <?php if(static::$auth->isAdmin()): ?>
            <div class="container">
                <div class="row addedit">
                    <div class="btn-group col-md-2 col-xs-3 col-xs-offset-9">
                        <a role="button" data-toggle="collapse" href="#addImages"
                        aria-expanded="false" aria-controls="addImages">
                            + Add Images
                        </a>
                        <br>
                        <a role="button" href=".\?page=editimages&amp;id=<?= $property->id; ?>">
                            + Edit Images
                        </a>                   
                    </div>
                </div>         
             </div>
            
            <br><br> 
            <div class="collapse" id="addImages">
                  <form id="addimages" action=".\?page=storeimages" method="POST" class="form-horizontal" enctype="multipart/form-data">        
                   
                    <div class="row">
                            <input type="hidden" name="property_id" value="<?= $_GET['id']; ?>">
                            <input type="hidden" name="count" value="<?= $count; ?>">
                            <div class="form-group">
                                <label for="image1" class=" col-sm-2 col-sm-offset-1 control-label">image1</label>
                                <div class="col-sm-4">
                                    <input type="file" class="" id="image1" name="image1">
                                </div>
                            </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="btn-group col-md-2 col-md-offset-5">
                              <button type="submit" class="btn btn-default submitbutton"><small>ADD IMAGES</small></button>
                            </div>
                        </div>
                   </div>
                  </form>
            </div>
       <?php endif; ?>

         <div class="propertyfocus property-lower">
            <div class="container">
                <div class="row ">
                    <div class="col-md-5 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
                    <div class="row">
                    <?php $count = 0; ?>

                    <?php if( $setValue !== true) : ?>

                        <?php if(count($imageDetails) > 0 ) : ?>
                        
                             <?php foreach ($imageDetails as $image) : ?>
                                <?php $count++; ?>
                                <?php if($count === 4): ?><div class="row top-buffer"></div><?php endif; ?> 
                                <div class="col-xs-4">
                                        <img src="./images/photos/150h/<?=$image->filename;?>"><br>
                                </div>                
                             <?php endforeach; ?> 
                        <?php endif; ?>

                    <?php else : ?>

                        <?php if(count($images) > 0 ) : ?>
                                                 
                            <?php foreach ($images as $image) : ?>

                                <?php if($image->filename != "") : ?>

                                    <?php $count++; ?>
                                    <?php if($count === 4): ?><div class="row top-buffer"></div><?php endif; ?> 
                                    <div class="col-xs-4">
                                            <img src="./images/photos/150h/<?=$image->filename;?>"><br>
                                            <form action=".\?page=updateimages" method="POST" class="form-horizontal" enctype="multipart/form-data">
                                                <input type="hidden" name="image_id" value="<?= $image->id; ?>">
                                                <span class="btn btn-default btn-file">
                                                    Browse <input type="file" name="image1">
                                                </span>
                                                <button type="submit" class="btn btn-default"><small>Edit</small></button> 
                                            </form>
                                            
                                    </div>   

                                <?php else: ?>
                                    <div class="col-xs-4">
                                        <p><i>No Photo found for this Property</i></p>
                                    </div>
                                <?php endif; ?>

                             <?php endforeach; ?> 

                            <?php endif; ?>

                        <?php endif; ?>

                
                    </div>    
                    
                    </div>
                    <div class="col-md-4 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
                        <div class="row">
                            <div class="col-xs-3 bed-bath">
                                <p><img src="images/bedroom_white.svg" onerror="this.src='images/png/bedroom_white.png'"> <?= $property->bedrooms; ?></p>
                            </div>
                            <div class="col-xs-3 bed-bath">
                                <p><img src="images/bathroom_white.svg" onerror="this.src='images/png/bathroom_white.png'"> <?= $property->bathrooms; ?></p>
                            </div>
                            <div class="col-xs-3 bed-bath">
                                <p><img src="images/land_white.svg" onerror="this.src='images/png/land_white.png'"> <?= $property->land; ?>m&#0178</p>
                            </div>
                            <div class="col-xs-3 bed-bath">
                                <p><img src="images/parking_white.svg" onerror="this.src='images/png/parking_white.png'"> <?= $property->parking; ?></p>
                            </div> 

                            <?php if(static::$auth->check()): ?>

                            <div class="btn-group col-xs-8 col-xs-offset-2">
                                <button type="button" class="btn btn-default watchbutton disabled"><small>+WATCHLIST (coming soon)</small></button>
                            </div>

                            <?php else: ?>
                                <br><br><br>
                                <div class="btn-group col-xs-12">
                                <h4 class="authentication-title"><a href="./?page=login"><i>log in </i></a><small class="white"> to add this property to your watchlist.</small></h4>
                                </div>
                            <?php endif; ?>

                        </div>   
                    </div>
                </div>
                <hr>
                <div>
                    <p class="centre"><i>If you would like more information on this property, please feel free to
                    <br> 
                    contact us through our contact sheet or email us at info@milwardrealty.com.</i></p>
                </div>
            </div>
        </div>



