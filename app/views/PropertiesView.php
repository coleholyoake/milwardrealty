<?php

namespace App\Views;

class PropertiesView extends TemplateView
{
	
	public function render()
	{
		extract($this->data);
		$page = "properties";
		$page_title = "";
		include "templates/master.inc.php";
	}
	protected function content()
	{
		extract($this->data);
		include "templates/properties.inc.php";
	}
	
}