<?php

namespace App\Views;

class AddPropertyView extends TemplateView
{
	
	public function render()
	{
		extract($this->data);
		$page = "addproperty";
		$page_title = "Add Property";
		include "templates/master.inc.php";
	}
	protected function content()
	{
		extract($this->data);
		include "templates/addproperty.inc.php";
	}
}