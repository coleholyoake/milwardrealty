<?php

namespace App\Views;

class HomeView extends TemplateView
{
	
	public function render()
	{
		extract($this->data);
		$page = "home";
		$page_title = "";
		include "templates/master.inc.php";
	}
	protected function content()
	{
		extract($this->data);
		
		include "templates/home.inc.php";
		include "templates/introduction.inc.php";
		include "templates/properties.inc.php";
		include "templates/about.inc.php";
	}

	
}