<?php

namespace App\Views;

class PropertyView extends TemplateView
{
	
	public function render()
	{
		extract($this->data);
		$page = "property";
		$page_title = "";
		include "templates/master.inc.php";
	}
	protected function content()
	{
		extract($this->data);
		include "templates/property.inc.php";
		include "templates/properties.inc.php";
	}
}