<?php

namespace App\Views;

class WatchlistView extends TemplateView
{
	
	public function render()
	{
		extract($this->data);
		$page = "watchlist";
		$page_title = "Watchlist";
		include "templates/master.inc.php";
	}
	protected function content()
	{
		extract($this->data);
		include "templates/watchlist.inc.php"; 
	}
}