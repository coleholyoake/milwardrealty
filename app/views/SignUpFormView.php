<?php

namespace App\Views;

class SignUpFormView extends TemplateView
{
	
	public function render()
	{
		extract($this->data);
		$page = "signup";
		$page_title = "Sign Up";
		include "templates/master.inc.php";
	}
	protected function content()
	{
		extract($this->data);
		include "templates/signup.inc.php"; 
	}
}