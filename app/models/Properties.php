<?php

namespace App\Models;
use PDO;
use finfo;
use Intervention\Image\ImageManagerStatic as Image;

class Properties extends DatabaseModel
{
	protected static $tableName = "properties";
	protected static $columns = ['id', 'region', 'address', 'price', 'description', 'bedrooms', 'bathrooms', 'land', 'parking', 'filename'];
	protected static $fakeColumns = ['image1'];
	protected static $validationRules = [
								"address"			=> "minlength:2",
								"region"			=> "minlength:2",
								"price"				=> "numeric,minlength:3",
								"description"		=> "minlength:10",
								"bedrooms"			=> "numeric,minlength:1",
								"bathrooms"			=> "numeric,minlength:1"
								
										];

	public static function search($searchQuery1, $searchQuery2)
	{
		
		$models = [];

		$db = static::getDataBaseConnection();


		$query = "
					SELECT id, address, price, description, bedrooms, bathrooms, region 
                    FROM properties
                    WHERE region = :searchQuery1 AND bedrooms = :searchQuery2";

		$statement = $db->prepare($query);
		$statement->bindValue(":searchQuery2", $searchQuery2);
		$statement->bindValue(":searchQuery1", $searchQuery1);
		// var_dump($statement);
		// die();
		$record = $statement->execute();
		// var_dump($record);
		while ($record = $statement->fetch(PDO::FETCH_ASSOC)) {
			$model = new Properties();
			$model->data = $record;
			array_push($models, $model);
		}

		return $models;
	}

	public function getImages()
	{
		$models = [];

		$db = static::getDataBaseConnection();
		$query = " SELECT id, filename FROM images ";
		$query .= " JOIN property_image ON id = image_id ";
		$query .= " WHERE property_id =:id";

		$statement = $db->prepare($query);
		$statement->bindValue(":id", $this->id);
		$statement->execute();

		while($record = $statement->fetch(PDO::FETCH_ASSOC)){
			$model = new Images();
			$model->data = $record;
			array_push($models, $model);
		}
		return $models;
	}
	 public function saveImageDetails()
	{
		$image1 = explode(",", $this->image1);
		foreach ($image1 as $img) {
			$img = trim($img);
		}
		$db = static::getDataBaseConnection();

		$db->beginTransaction();

		try {
			$this->addNewImagesToProperty($db, $image1);
			// $imageIds = $this->getImageIds($db, $image1);
			// $this->deleteImageFromProperty($db);
			// $this->insertImageForProperty($db, $imageIds);
			

			$db->commit();

		} catch (Exception $e) {
			$db->rollback();
			throw $e;
		}
	}
	private function addNewImagesToProperty($db, $image1)
	{
		// insert each tag into the tags table (ignore all duplicates)
		$query = "INSERT IGNORE INTO images (filename) VALUES ";

		$imagevalues = [];
			for ($i=0; $i < count($image1); $i++) {
						//method - Interpolation
				array_push($imagevalues, "(:img{$i})");
			}
			$query .= implode(",", $imagevalues);
			$statement = $db->prepare($query);
			for ($i=0; $i < count($image1); $i++) {
				$statement->bindValue(":img{$i}", $image1[$i]);
			}
			$statement->execute();
	}
	
	private function getImageIds($db, $image)
	{
		//getting the Id for each tags
		$query = "SELECT id FROM images WHERE ";
		$imagevalues = [];
		for ($i=0; $i < count($image); $i++) { 
			array_push($imagevalues, "img = :img{$i}");
		}
		$query .= implode(" OR " , $imagevalues);
		$statement = $db->prepare($query);
		for ($i=0; $i < count($image1); $i++) {
			$statement->bindValue(":img{$i}", $image1[$i]);
		}
		$statement->execute();

		$record = $statement->fetchAll(PDO::FETCH_COLUMN);
		return $record;
	}
	private function insertImageForProperty($db, $imageIds)
	{
		$query = "INSERT IGNORE INTO property_image (property_id, image_id) VALUES ";

		$imagevalues = [];
		for ($i=0; $i < count($imageIds); $i++) { 
			array_push($imagevalues, "(:property_id_{$i}, :image_id_{$i})");
		}
		$query .= implode(",", $imagevalues);
		$statement = $db->prepare($query);
		for ($i=0; $i < count($imageIds); $i++) { 
			$statement->bindValue(":property_id_{$i}", $this->id);
			$statement->bindValue(":image_id_{$i}", $imageIds[$i]);
		}
		$statement->execute();
	}
	private function deleteImageFromProperty($db)
	{
		$query = "DELETE FROM property_tag WHERE property_id= :property_id";
		$statement = $db->prepare($query);
		$statement->bindValue(":property_id", $this->id);
		$statement->execute();
	}

}

