<?php

namespace App\Models;
use PDO;
use finfo;
use Intervention\Image\ImageManagerStatic as Image;

class Images extends DatabaseModel
{
	protected static $tableName = "images";
	protected static $columns = ['id', 'filename', 'property_id', 'count'];

}