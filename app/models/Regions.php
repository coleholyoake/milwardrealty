<?php

namespace App\Models;

class Regions extends DatabaseModel
{
	protected static $tableName = "regions";
	protected static $columns = ['id', 'region', 'description'];
}