<?php  

namespace App\Controllers;

use App\Views\WatchlistView;

class WatchlistController extends Controller
{
	public function show()
	{
		$view = new WatchlistView();
		$view->render();
	}
	
}