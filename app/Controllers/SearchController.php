<?php  

namespace App\Controllers;

use App\Views\SearchResultsView;
use App\Models\Properties;


class SearchController extends Controller
{
	public function search()
	{

		$region = isset($_GET['region']) ? $_GET['region'] : null;
		$bedrooms = isset($_GET['bedrooms']) ? $_GET['bedrooms'] : null;
		
		$properties = Properties::search($region, $bedrooms);	
		$view = new SearchResultsView(compact('properties')); 
		$view->render();
	}
	
}