<?php  

namespace App\Controllers;

use App\Views\HomeView;
use App\Views\PropertyView;
use App\Views\PropertiesView;
use App\Models\Properties;
use App\Models\Regions;

class HomeController extends Controller
{
	public function show()
	{
		$region = isset($_GET['region']) ? $_GET['region'] : "TAURANGA";
		

		$regionDetails = Regions::findBy('region', $region);
		$regionID = $regionDetails->region;
		

		$pageNumber = isset($_GET['p']) ? $_GET['p'] : 1;
		$pageSize = 10;

		$propertyDetails = Properties::all('region', true, $pageNumber, $pageSize, $regionID);
		
		$view = new HomeView(compact('regionDetails', 'propertyDetails'));
		$view->render();
	}
	
}