<?php  

namespace App\Controllers;

use App\Views\PropertiesView;
use App\Views\PropertyView;
use App\Views\AddPropertyView;
use App\Views\AddImagesView;
use App\Models\Properties;
use App\Models\Regions;
use App\Models\Images;


class PropertyController extends Controller
{
	public function index()
	{	
		$region=$_GET['region'];
		$view = new PropertiesView(compact('regions', 'properties'));
		$view->render();
	}

	public function show()
	{
		$id = $_GET['id'];
		$setValue = false;
		$property = new Properties((int)$_GET['id']);

		$imageDetails = Images::allBy('property_id', $id);
		
		$view = new PropertyView(compact('property','imageDetails', 'setValue'));
		$view->render();

	}

	public function add()
	{
		static::$auth->mustBeAdmin();

		$property= $this->getFormData();
		$view = new AddPropertyView(['property' => $property]);
		$view->render();

	}

	public function store()
	{
		static::$auth->mustBeAdmin();

		$property = new Properties($_POST);

		if(! $property->isValid()){
			$_SESSION["addproperty"] = $property;
			header("Location: ./?page=addproperty");
			exit();
		}
		
		if($_FILES['filename']['error'] === UPLOAD_ERR_OK){
			$property->savePhoto($_FILES['filename']['tmp_name']);
		}

		$property->save();

		header("Location: ./?page=property&id=" . $property->id);
	}
	public function storeimages()
	{
		$count = $_POST['count'];

		static::$auth->mustBeAdmin();

		$image = new Images($_POST);
		
		if($_FILES['image1']['error'] === UPLOAD_ERR_OK){
			$image->savePhoto($_FILES['image1']['tmp_name']);
		}
		if($count < 7) {
			$image->save();
		}
		header("Location: ./?page=property&id=" . $image->property_id . "&count=" . $image->count);
	}

	public function edit()
	{
		static::$auth->mustBeAdmin();

		$property = $this->getFormData($_GET['id']);
	
		$view = new AddPropertyView(compact('property'));
		$view->render();

	}
	public function editimages()
	{
		static::$auth->mustBeAdmin();

		$id = $_GET['id'];
		$images = Images::allBy('property_id',$id);
		$setValue = true;
		$property = new Properties((int)$_GET['id']);
		$imageDetails = Images::allBy('property_id', $id);

		$view = new PropertyView(compact('images', 'setValue', 'property', 'imageDetails'));
		$view->render();

	}

	public function update()
	{
		static::$auth->mustBeAdmin();

		$property = new Properties($_POST['id']);
		$property->processArray($_POST);

		if(! $property->isValid()){
			$_SESSION["addproperty"] = $property;
			header("Location: ./?page=editproperty&id=" . $_POST['id']);
			exit();
		}

		if($_FILES['filename']['error'] === UPLOAD_ERR_OK){
			$property->savePhoto($_FILES['filename']['tmp_name']);
		}
		
		$property->save();

		header("Location: ./?page=property&id=" . $property->id);
	}
	public function updateimages()
	{
		static::$auth->mustBeAdmin();
		
		$image = Images::findBy('id', $_POST['image_id']);
		
		if($_FILES['image1']['error'] === UPLOAD_ERR_OK){
			$image->savePhoto($_FILES['image1']['tmp_name']);
		}else if(isset($_POST['removeImage']) && $_POST['removeImage'] === "true") {
			$image->filename = null;
		}
		$image->save();
		header("Location: ./?page=property&id=" . $image->property_id);
	}

	public function destroy()
	{	
		static::$auth->mustBeAdmin();
		
		Properties::destroy($_POST['id']);
		header("Location: ./?page=home#region-nav");
	}

	public function getFormData($id = null)
	{
		if(isset($_SESSION['addproperty'])){
			$property = $_SESSION['addproperty'];
			unset($_SESSION['addproperty']);
		} else {
			$property = new Properties((int)$id);
		}
		return $property;
	} 

	
	
}