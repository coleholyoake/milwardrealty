<?php 

namespace App\Controllers;

use App\Views\SignUpFormView;
use App\Views\LogInFormView;
use App\Models\User;

class AuthenticationController extends Controller
{
	public function signup()
	{
		$user = $this->getUserFormData();
		$view = new SignUpformView(compact('user'));
		$view->render();
	}
	public function store()
	{
		$user = new User($_POST);
		if(! $user->isValid()){
			$_SESSION['user.form'] = $user;
			header("Location:./?page=signup");
			exit();
		}
		$user->save();
		header("Location:./?page=login");
	}
	public function login()
	{	
		$user = $this->getUserFormData();
		$error = isset($_GET['error']) ? $_GET['error'] : null;
		$view = new LoginFormView(compact('user', 'error'));
		$view->render();
	}
	public function attempt() 
	{
		var_dump($_POST);
		if(static::$auth->attempt($_POST['email'],$_POST['password'])){
			//log in is successful
			header("Location:./");
			exit();
		} 
		header("Location:./?page=login&error=true");
		exit();
	}
	public function logout()
	{
		static::$auth->logout();
		header("Location:./?page=login");
	}
	protected function getUserFormData($id = NULL)
	{
		if(isset($_SESSION['user.form'])){
			$user = $_SESSION['user.form'];
			unset($_SESSION['user.form']);
		} else {
			$user = new User($id);
		}
		return $user;
	}
}