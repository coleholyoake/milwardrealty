<?php  

namespace App\Controllers;

Class Controller
{
	protected static $auth;

	public static function registerAuthenticationService($auth)
	{
		self::$auth = $auth;
	}
}