$( document ).ready(function() {


	$('.more').on('click', function (e){
		e.preventDefault();

		var target = this.hash;
		var $target = $(target);

		$('html, body').stop().animate({
			'scrollTop': $target.offset().top
		}, 900, 'swing', function () {
			window.location.hash = target;
		
		});
	});

	  var data = [{
        value: 15,
        color: "#fff",
        label: "TAURANGA"
    }, {
        value: 24,
        color: "#e7e3e4",
        label: "MOUNT MAUNGANUI"
    }, {
        value: 12,
        color: "#c5c1c2",
        label: "WHAKATANE"
    }, {
        value: 4,
        color: "#9e9a9b",
        label: "KATIKATI"
    }, {
        value: 7,
        color: "#787274",
        label: "KAWERAU"
    }, {
        value: 2,
        color: "#484344",
        label: "TE PUKE"
    }

    ]

    var options = {
        animation: true,
        segmentShowStroke: false
        
    };

    //Get the context of the canvas element we want to select
    var c = $('#myChart');
    var ct = c.get(0).getContext('2d');
    var ctx = document.getElementById("myChart").getContext("2d");
    /*************************************************************************/
    myNewChart = new Chart(ctx).Doughnut(data, options);
});



