<?php

namespace App\Controllers;
use App\Models\Exceptions\ModelNotFoundException;
use App\Services\Exceptions\InsufficientPrivilegesExceptions;

$page =isset($_GET['page']) ? $_GET['page'] :'home';

try {
	switch ($page) {
		case 'home':
			$controller = new HomeController();
			$controller-> show();
		break;

		case 'signup':
			$controller = new AuthenticationController();
			$controller-> signup();
		break;

		case 'login':
			$controller = new AuthenticationController();
			$controller-> login();
		break;

		case "auth.store":
			$controller = new AuthenticationController();
			$controller->store();
		break;

		case 'logout':
			$controller = new AuthenticationController();
			$controller-> logout();
			break;			

		case 'auth.attempt':
			$controller = new AuthenticationController();
			$controller-> attempt();
			break;

		case 'introduction':
			$controller = new IntroductionController();
			$controller-> show();
		break;

		case "search":
			$controller = new SearchController();
			$controller->search();
			break;

		case 'properties':
			$controller = new PropertyController();
			$controller-> index();
		break;

		case 'property':
			$controller = new PropertyController();
			$controller-> show();
		break;

		case 'addproperty':
			$controller = new PropertyController();
			$controller-> add();
		break;
		case 'addimages':
			$controller = new PropertyController();
			$controller-> addimages();
		break;

		case 'storeproperty':
			$controller = new PropertyController();
			$controller-> store();
		break;
		case 'storeimages':
			$controller = new PropertyController();
			$controller-> storeimages();
		break;

		case 'editproperty':
			$controller = new PropertyController();
			$controller-> edit();
			break;
		case 'editimages':
			$controller = new PropertyController();
			$controller-> editimages();
			break;

		case 'updateproperty':
			$controller = new PropertyController();
			$controller-> update();
			break;
		case 'updateimages':
			$controller = new PropertyController();
			$controller-> updateimages();
			break;

		case 'deleteproperty':
			$controller = new PropertyController();
			$controller-> destroy();
			break;

		case 'contact':
			$controller = new ContactController();
			$controller-> show();
		break;

		case 'watchlist':
			$controller = new WatchlistController();
			$controller-> show();
		break;

		default:
			throw new ModelNotFoundException();
		break;
	}

} catch(ModelNotFoundException $e)
	{
		$controller = new ErrorController();
		$controller->error404();
}	catch(InsufficientPrivilegesExceptions $e)
	{
		$controller = new ErrorController();
		$controller->error401();
}
		